import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#00aef2"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                color: "#000000"
                text: qsTr("Pluie")
                font.pointSize: 14
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
            }
        }

        Image {
            id: imageRain1
            width: 75
            height: 75
            fillMode: Image.PreserveAspectFit
            source: "weather_icon_pack/png/rain-gauge.png"
            anchors {
                top: parent.top
                topMargin: parent.height * 0.2
                left: parent.left
                leftMargin: parent.width * 0.1
            }
        }

        Image {
            id: imageRain2
            width: 75
            height: 75
            fillMode: Image.PreserveAspectFit
            source: "weather_icon_pack/png/rain-gauge.png"
            anchors {
                top: parent.top
                topMargin: parent.height * 0.2
                right: parent.right
                rightMargin: parent.width * 0.1
            }
        }

        Label {
            id: labelTotalRainToday
            color: "#00aef2"
            text: qsTr("Total aujourd'hui ")
            font.pointSize: 14
            anchors {
                top: imageRain1.bottom
                topMargin: parent.height * 0.01
                horizontalCenter: imageRain1.horizontalCenter
            }
        }

        Label {
            id: labelTotalRainYesterday
            color: "#00aef2"
            text: qsTr("Total hier")
            font.pointSize: 14
            anchors {
                top: imageRain2.bottom
                topMargin: parent.height * 0.01
                horizontalCenter: imageRain2.horizontalCenter
            }
        }

        Label {
            id: valueTotalRainToday
            color: "#ffffff"
            text: total_rainfall + qsTr(" mm")
            font.pointSize: 16
            anchors {
                top: labelTotalRainToday.bottom
                horizontalCenter: labelTotalRainToday.horizontalCenter
                horizontalCenterOffset: -12
            }
        }

        Label {
            id: valueTotalRainYesterday
            color: "#ffffff"
            text: yesterday_rainfall + qsTr(" mm")
            font.pointSize: 16
            anchors {
                top: labelTotalRainYesterday.bottom
                horizontalCenter: labelTotalRainYesterday.horizontalCenter
                horizontalCenterOffset: -12
            }
        }

        Label {
            id: labelRainLevel
            color: "#00aef2"
            text: qsTr("Précipitations : ")
            font.pointSize: 14
            anchors {
                top: valueTotalRainToday.bottom
                topMargin: 10
                left: parent.left
                leftMargin: parent.width * 0.05
            }
        }

        Label {
            id: labelRainLevelValue
            width: 240
            height: 23
            color: "#ffffff"
            text: rain_level + qsTr(" mm/hr")
            font.pointSize: 14
            anchors {
                left: labelRainLevel.right
                leftMargin: 6
                verticalCenter: labelRainLevel.verticalCenter
            }
        }
    }
}
