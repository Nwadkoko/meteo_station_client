QT += quick widgets
CONFIG += c++14

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
    tcp.cpp \
    wmr928.cpp \
    datahandler.cpp \
    udp.cpp

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML2_IMPORT_PATH = "/home/matthieu/Qt/5.12.2/gcc_64/qml/"

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    frame_info.h \
    tcp.h \
    wmr928.h \
    datahandler.h \
    udp.h
