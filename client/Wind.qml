import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        color: "#494949"
        radius: 10
    }

    Rectangle {
        id: rectangleTitle
        color: "#00aef2"
        radius: 10
        width: parent.width
        height: 25
        anchors {
            top: parent.top
        }

        Label {
            id: labelTemperatureModule
            color: "#000000"
            text: qsTr("Vents")
            font.pointSize: 14
            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }

    }

    Label {
        id: labelDirectionValue
        x: 227
        color: "#ffffff"
        text: arrow_angle + qsTr(" °")
        anchors.top: labelDirection.bottom
        anchors.topMargin: -1
        font.pointSize: 14
    }

    Label {
        id: labelWindSpeedAverageValue
        x: 227
        color: "#ffffff"
        text: wind_speed_average + qsTr(" m/s")
        anchors.top: labelWindSpeedAverage.bottom
        anchors.topMargin: -2
        font.pointSize: 14
    }

    Label {
        id: labelDirection
        x: 227
        y: 29
        color: "#00aef2"
        text: "Direction"
        anchors.leftMargin: 6
        font.pointSize: 14
    }

    Label {
        id: labelWindSpeedAverage
        x: 227
        y: 80
        color: "#00aef2"
        text: "Vitesse moyenne"
        anchors.leftMargin: 6
        font.pointSize: 14
    }

    CircularGauge {
        id: circularGauge
        x: 8
        y: 29
        width: 199
        height: 208
        minimumValue: 0
        maximumValue: 360
        value: arrow_angle
        style: CircularGaugeStyle {
            //labelStepSize: 25
            labelStepSize: 0
            minimumValueAngle: 0
            maximumValueAngle: 360
            tickmarkStepSize: 45
            needle: Rectangle {
                y: outerRadius * 0.15
                implicitWidth: outerRadius * 0.03
                implicitHeight: outerRadius * 0.9
                antialiasing: true
                color: "#000000"

            }
        }
    }
}

