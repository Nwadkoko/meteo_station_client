import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        width: 400
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#fb0000"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                x: 104
                y: 0
                color: "#000000"
                text: qsTr("Configuration connexion")
                font.pointSize: 14
            }
        }

        Label {
            id: labelIP
            text: "Adresse IP : "
            font.pointSize: 14
            anchors {
                top: parent.top
                topMargin: 35
                left: parent.left
                leftMargin: 5
            }
        }

        TextEdit {
            id: valueIP
            text: "127.0.0.1"
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: labelIP.right
                leftMargin: 15
                verticalCenter: labelIP.verticalCenter
            }
        }

        Label {
            id: labelPort
            text: "Port : "
            font.pointSize: 14
            anchors {
                top: labelIP.top
                topMargin: 20
                left: parent.left
                leftMargin: 5
            }
        }

        TextEdit {
            id: valuePort
            text: "37000"
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: labelPort.right
                leftMargin: 15
                verticalCenter: labelPort.verticalCenter
            }
        }

        Button {
            text: "Envoyer"
            anchors {
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 90
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: 155
            }
            onClicked: {
                dataHandler.sendIP(valuePort.text, valueIP.text);
                m_connection.visible = false;
                m_barometer.visible = true;
                m_barstatus.visible = true;
                m_configurationalarm.visible = true;
                m_configurationtemp.visible = true;
                m_configurationwindrain.visible = true;
                m_insidetemp.visible = true;
                m_outsidetemp.visible = true;
                m_rain.visible = true;
                m_wind.visible = true;
            }
        }
    }
}
