import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4

Item {
    id: element
    height: parent.height * 0.15
    width: parent.width * 0.33
    anchors {
        bottom: parent.bottom
        horizontalCenter: parent.horizontalCenter
    }

    Label {
        id: labelMushroomLow
        color: "#ffffff"
        text: qsTr("Thermomètre extérieure")
        anchors.horizontalCenterOffset: -16
        anchors {
            bottom: labelTHB2Low.top
            bottomMargin: 20
            horizontalCenter: parent.horizontalCenter
        }

    }

    StatusIndicator {
        id: statusIndicatorMushroom
        active: mushroom_is_low
        anchors {
            bottom: labelTHB2Low.top
            bottomMargin: 20
            left: labelMushroomLow.right
            leftMargin: 5
        }
    }

    Label {
        id: labelTHB2Low
        color: "#ffffff"
        text: qsTr("Thermomètre intérieure")
        anchors.horizontalCenterOffset: -16
        anchors {
            bottom: labelWindLowBatt.top
            bottomMargin: 20
            horizontalCenter: parent.horizontalCenter
        }

    }

    StatusIndicator {
        id: statusIndicatorTHB2Low
        active: tHB2_is_low
        anchors {
            bottom: labelWindLowBatt.top
            bottomMargin: 20
            left: labelTHB2Low.right
            leftMargin: 5
        }
    }

    StatusIndicator {
        id: statusIndicatorLowBatt
        active: wind_is_low
        anchors {
            bottom: labelRainLowBatt.top
            bottomMargin: 20
            left: labelWindLowBatt.right
            leftMargin: 5
        }
    }

    Label {
        id: labelWindLowBatt
        color: "#ffffff"
        text: qsTr("Anénomètre - Girouette")
        anchors.horizontalCenterOffset: -16
        anchors {
            bottom: labelRainLowBatt.top
            bottomMargin: 20
            horizontalCenter: parent.horizontalCenter
        }
    }

    StatusIndicator {
        id: statusIndicatorRainLowBatt
        x: 77
        y: 42
        active: rain_is_low
        anchors {
            bottom: labelIpAddress.top
            bottomMargin: 10
            left: labelRainLowBatt.right
            leftMargin: 5
        }
    }

    Label {
        id: labelRainLowBatt
        color: "#ffffff"
        text: qsTr("Pluviomètre")
        anchors.verticalCenterOffset: 51
        anchors {
            bottom: labelIpAddress.top
            bottomMargin: 10
            horizontalCenter: parent.horizontalCenter
        }
    }

    Label {
        id: labelIpAddress
        color: "#ffffff"
        text: "Adresse IP : " + ip_address
        anchors {
            bottom: progressBar.top
            bottomMargin: 10
            horizontalCenter: parent.horizontalCenter
        }
    }

    ProgressBar {
        id: progressBar
        anchors.verticalCenterOffset: 170
        value: valueProgressBar
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }
}
