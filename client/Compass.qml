import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4

Item {
    id: element
    width: 400
    height: 200

    Rectangle {
        id: rectangle
        x: 0
        y: 0
        width: 400
        height: 200
        color: "#ffffff"
        radius: 10
    }

    Rectangle {
        id: rectangle1
        width: 200
        height: 200

        Image {
            source: "compass_png/compass.png"
            anchors.fill: parent
        }

        Image {
            id: arrow
            source: "compass_png/compass-arrow.png"
            anchors.centerIn: parent
            width: 93
            height: 200
            antialiasing: true
            anchors.verticalCenterOffset: 0
            anchors.horizontalCenterOffset: 6
            rotation: arrow_angle
        }
    }

    CircularGauge {
        id: circularGauge
        x: 200
        y: 0
        width: 192
        height: 200
    }
}
