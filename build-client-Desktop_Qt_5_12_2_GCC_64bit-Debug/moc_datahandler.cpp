/****************************************************************************
** Meta object code from reading C++ file 'datahandler.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../client/datahandler.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datahandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_data_t {
    QByteArrayData data[23];
    char stringdata0[355];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_data_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_data_t qt_meta_stringdata_data = {
    {
QT_MOC_LITERAL(0, 0, 4), // "data"
QT_MOC_LITERAL(1, 5, 18), // "inside_temperature"
QT_MOC_LITERAL(2, 24, 15), // "inside_dew_temp"
QT_MOC_LITERAL(3, 40, 19), // "outside_temperature"
QT_MOC_LITERAL(4, 60, 16), // "outside_dew_temp"
QT_MOC_LITERAL(5, 77, 10), // "wind_chill"
QT_MOC_LITERAL(6, 88, 15), // "inside_humidity"
QT_MOC_LITERAL(7, 104, 16), // "outside_humidity"
QT_MOC_LITERAL(8, 121, 14), // "wind_direction"
QT_MOC_LITERAL(9, 136, 15), // "wind_speed_gust"
QT_MOC_LITERAL(10, 152, 18), // "wind_speed_average"
QT_MOC_LITERAL(11, 171, 4), // "rain"
QT_MOC_LITERAL(12, 176, 14), // "total_rainfall"
QT_MOC_LITERAL(13, 191, 18), // "yesterday_rainfall"
QT_MOC_LITERAL(14, 210, 8), // "pressure"
QT_MOC_LITERAL(15, 219, 11), // "windLowBatt"
QT_MOC_LITERAL(16, 231, 11), // "rainLowBatt"
QT_MOC_LITERAL(17, 243, 18), // "thermoHygroLowBatt"
QT_MOC_LITERAL(18, 262, 15), // "mushroomLowBatt"
QT_MOC_LITERAL(19, 278, 17), // "thermoOnlyLowBatt"
QT_MOC_LITERAL(20, 296, 22), // "thermoHygroBaroLowBatt"
QT_MOC_LITERAL(21, 319, 11), // "mainLowBatt"
QT_MOC_LITERAL(22, 331, 23) // "thermoHygroBaro2LowBatt"

    },
    "data\0inside_temperature\0inside_dew_temp\0"
    "outside_temperature\0outside_dew_temp\0"
    "wind_chill\0inside_humidity\0outside_humidity\0"
    "wind_direction\0wind_speed_gust\0"
    "wind_speed_average\0rain\0total_rainfall\0"
    "yesterday_rainfall\0pressure\0windLowBatt\0"
    "rainLowBatt\0thermoHygroLowBatt\0"
    "mushroomLowBatt\0thermoOnlyLowBatt\0"
    "thermoHygroBaroLowBatt\0mainLowBatt\0"
    "thermoHygroBaro2LowBatt"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_data[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
      22,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Float, 0x00095003,
       2, QMetaType::Float, 0x00095003,
       3, QMetaType::Float, 0x00095003,
       4, QMetaType::Float, 0x00095003,
       5, QMetaType::Float, 0x00095003,
       6, QMetaType::Float, 0x00095003,
       7, QMetaType::Float, 0x00095003,
       8, QMetaType::Float, 0x00095003,
       9, QMetaType::Float, 0x00095003,
      10, QMetaType::Float, 0x00095003,
      11, QMetaType::Float, 0x00095003,
      12, QMetaType::Float, 0x00095003,
      13, QMetaType::Float, 0x00095003,
      14, QMetaType::Float, 0x00095003,
      15, QMetaType::Bool, 0x00095003,
      16, QMetaType::Bool, 0x00095003,
      17, QMetaType::Bool, 0x00095003,
      18, QMetaType::Bool, 0x00095003,
      19, QMetaType::Bool, 0x00095003,
      20, QMetaType::Bool, 0x00095003,
      21, QMetaType::Bool, 0x00095003,
      22, QMetaType::Bool, 0x00095003,

       0        // eod
};

void data::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        data *_t = reinterpret_cast<data *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< float*>(_v) = _t->m_inside_temperature; break;
        case 1: *reinterpret_cast< float*>(_v) = _t->m_inside_dew_temp; break;
        case 2: *reinterpret_cast< float*>(_v) = _t->m_outside_temperature; break;
        case 3: *reinterpret_cast< float*>(_v) = _t->m_outside_dew_temp; break;
        case 4: *reinterpret_cast< float*>(_v) = _t->m_wind_chill; break;
        case 5: *reinterpret_cast< float*>(_v) = _t->m_inside_humidity; break;
        case 6: *reinterpret_cast< float*>(_v) = _t->m_outside_humidity; break;
        case 7: *reinterpret_cast< float*>(_v) = _t->m_wind_direction; break;
        case 8: *reinterpret_cast< float*>(_v) = _t->m_wind_speed_gust; break;
        case 9: *reinterpret_cast< float*>(_v) = _t->m_wind_speed_average; break;
        case 10: *reinterpret_cast< float*>(_v) = _t->m_rain; break;
        case 11: *reinterpret_cast< float*>(_v) = _t->m_total_rainfall; break;
        case 12: *reinterpret_cast< float*>(_v) = _t->m_yesterday_rainfall; break;
        case 13: *reinterpret_cast< float*>(_v) = _t->m_pressure; break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->m_windLowBatt; break;
        case 15: *reinterpret_cast< bool*>(_v) = _t->m_rainLowBatt; break;
        case 16: *reinterpret_cast< bool*>(_v) = _t->m_thermoHygroLowBatt; break;
        case 17: *reinterpret_cast< bool*>(_v) = _t->m_mushroomLowBatt; break;
        case 18: *reinterpret_cast< bool*>(_v) = _t->m_thermoOnlyLowBatt; break;
        case 19: *reinterpret_cast< bool*>(_v) = _t->m_thermoHygroBaroLowBatt; break;
        case 20: *reinterpret_cast< bool*>(_v) = _t->m_mainLowBatt; break;
        case 21: *reinterpret_cast< bool*>(_v) = _t->m_thermoHygroBaro2LowBatt; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        data *_t = reinterpret_cast<data *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0:
            if (_t->m_inside_temperature != *reinterpret_cast< float*>(_v)) {
                _t->m_inside_temperature = *reinterpret_cast< float*>(_v);
            }
            break;
        case 1:
            if (_t->m_inside_dew_temp != *reinterpret_cast< float*>(_v)) {
                _t->m_inside_dew_temp = *reinterpret_cast< float*>(_v);
            }
            break;
        case 2:
            if (_t->m_outside_temperature != *reinterpret_cast< float*>(_v)) {
                _t->m_outside_temperature = *reinterpret_cast< float*>(_v);
            }
            break;
        case 3:
            if (_t->m_outside_dew_temp != *reinterpret_cast< float*>(_v)) {
                _t->m_outside_dew_temp = *reinterpret_cast< float*>(_v);
            }
            break;
        case 4:
            if (_t->m_wind_chill != *reinterpret_cast< float*>(_v)) {
                _t->m_wind_chill = *reinterpret_cast< float*>(_v);
            }
            break;
        case 5:
            if (_t->m_inside_humidity != *reinterpret_cast< float*>(_v)) {
                _t->m_inside_humidity = *reinterpret_cast< float*>(_v);
            }
            break;
        case 6:
            if (_t->m_outside_humidity != *reinterpret_cast< float*>(_v)) {
                _t->m_outside_humidity = *reinterpret_cast< float*>(_v);
            }
            break;
        case 7:
            if (_t->m_wind_direction != *reinterpret_cast< float*>(_v)) {
                _t->m_wind_direction = *reinterpret_cast< float*>(_v);
            }
            break;
        case 8:
            if (_t->m_wind_speed_gust != *reinterpret_cast< float*>(_v)) {
                _t->m_wind_speed_gust = *reinterpret_cast< float*>(_v);
            }
            break;
        case 9:
            if (_t->m_wind_speed_average != *reinterpret_cast< float*>(_v)) {
                _t->m_wind_speed_average = *reinterpret_cast< float*>(_v);
            }
            break;
        case 10:
            if (_t->m_rain != *reinterpret_cast< float*>(_v)) {
                _t->m_rain = *reinterpret_cast< float*>(_v);
            }
            break;
        case 11:
            if (_t->m_total_rainfall != *reinterpret_cast< float*>(_v)) {
                _t->m_total_rainfall = *reinterpret_cast< float*>(_v);
            }
            break;
        case 12:
            if (_t->m_yesterday_rainfall != *reinterpret_cast< float*>(_v)) {
                _t->m_yesterday_rainfall = *reinterpret_cast< float*>(_v);
            }
            break;
        case 13:
            if (_t->m_pressure != *reinterpret_cast< float*>(_v)) {
                _t->m_pressure = *reinterpret_cast< float*>(_v);
            }
            break;
        case 14:
            if (_t->m_windLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_windLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 15:
            if (_t->m_rainLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_rainLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 16:
            if (_t->m_thermoHygroLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_thermoHygroLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 17:
            if (_t->m_mushroomLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_mushroomLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 18:
            if (_t->m_thermoOnlyLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_thermoOnlyLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 19:
            if (_t->m_thermoHygroBaroLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_thermoHygroBaroLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 20:
            if (_t->m_mainLowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_mainLowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        case 21:
            if (_t->m_thermoHygroBaro2LowBatt != *reinterpret_cast< bool*>(_v)) {
                _t->m_thermoHygroBaro2LowBatt = *reinterpret_cast< bool*>(_v);
            }
            break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject data::staticMetaObject = {
    { nullptr, qt_meta_stringdata_data.data,
      qt_meta_data_data,  qt_static_metacall, nullptr, nullptr}
};

struct qt_meta_stringdata_DataHandler_t {
    QByteArrayData data[25];
    char stringdata0[232];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DataHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DataHandler_t qt_meta_stringdata_DataHandler = {
    {
QT_MOC_LITERAL(0, 0, 11), // "DataHandler"
QT_MOC_LITERAL(1, 12, 18), // "currentDataChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 8), // "getFrame"
QT_MOC_LITERAL(4, 41, 26), // "std::vector<unsigned char>"
QT_MOC_LITERAL(5, 68, 5), // "frame"
QT_MOC_LITERAL(6, 74, 9), // "gen_crc16"
QT_MOC_LITERAL(7, 84, 8), // "uint16_t"
QT_MOC_LITERAL(8, 93, 11), // "const char*"
QT_MOC_LITERAL(9, 105, 4), // "data"
QT_MOC_LITERAL(10, 110, 4), // "size"
QT_MOC_LITERAL(11, 115, 11), // "getPressure"
QT_MOC_LITERAL(12, 127, 5), // "hours"
QT_MOC_LITERAL(13, 133, 10), // "sendConfig"
QT_MOC_LITERAL(14, 144, 8), // "numFrame"
QT_MOC_LITERAL(15, 153, 8), // "numField"
QT_MOC_LITERAL(16, 162, 10), // "dataToSend"
QT_MOC_LITERAL(17, 173, 3), // "min"
QT_MOC_LITERAL(18, 177, 3), // "max"
QT_MOC_LITERAL(19, 181, 10), // "alarmValue"
QT_MOC_LITERAL(20, 192, 6), // "sendIP"
QT_MOC_LITERAL(21, 199, 4), // "port"
QT_MOC_LITERAL(22, 204, 2), // "IP"
QT_MOC_LITERAL(23, 207, 11), // "isReceiving"
QT_MOC_LITERAL(24, 219, 12) // "current_data"

    },
    "DataHandler\0currentDataChanged\0\0"
    "getFrame\0std::vector<unsigned char>\0"
    "frame\0gen_crc16\0uint16_t\0const char*\0"
    "data\0size\0getPressure\0hours\0sendConfig\0"
    "numFrame\0numField\0dataToSend\0min\0max\0"
    "alarmValue\0sendIP\0port\0IP\0isReceiving\0"
    "current_data"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DataHandler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       1,  106, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    1,   65,    2, 0x0a /* Public */,
       6,    2,   68,    2, 0x0a /* Public */,
      11,    1,   73,    2, 0x0a /* Public */,
      13,    3,   76,    2, 0x0a /* Public */,
      13,    4,   83,    2, 0x0a /* Public */,
      13,    1,   92,    2, 0x0a /* Public */,
      13,    2,   95,    2, 0x0a /* Public */,
      20,    2,  100,    2, 0x0a /* Public */,
      23,    0,  105,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 4,    5,
    0x80000000 | 7, 0x80000000 | 8, 0x80000000 | 7,    9,   10,
    QMetaType::Float, QMetaType::Int,   12,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Int,   14,   15,   16,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Double, QMetaType::Double,   14,   15,   17,   18,
    QMetaType::Void, QMetaType::Int,   19,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   14,   15,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   21,   22,
    QMetaType::Bool,

 // properties: name, type, flags
      24, 0x80000000 | 9, 0x00495009,

 // properties: notify_signal_id
       0,

       0        // eod
};

void DataHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DataHandler *_t = static_cast<DataHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->currentDataChanged(); break;
        case 1: _t->getFrame((*reinterpret_cast< std::vector<unsigned char>(*)>(_a[1]))); break;
        case 2: { uint16_t _r = _t->gen_crc16((*reinterpret_cast< const char*(*)>(_a[1])),(*reinterpret_cast< uint16_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< uint16_t*>(_a[0]) = std::move(_r); }  break;
        case 3: { float _r = _t->getPressure((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< float*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->sendConfig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: _t->sendConfig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< double(*)>(_a[4]))); break;
        case 6: _t->sendConfig((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->sendConfig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->sendIP((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: { bool _r = _t->isReceiving();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DataHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DataHandler::currentDataChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< data >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        DataHandler *_t = static_cast<DataHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< data*>(_v) = _t->getCurrentData(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject DataHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_DataHandler.data,
      qt_meta_data_DataHandler,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *DataHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DataHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DataHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int DataHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void DataHandler::currentDataChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
