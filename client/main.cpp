#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickView>

#include "datahandler.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QCoreApplication::setOrganizationName("2TSSNIR");
    QCoreApplication::setApplicationName("Station Meteo");

    QApplication app(argc, argv);

    DataHandler dataHandler;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("dataHandler", &dataHandler);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
