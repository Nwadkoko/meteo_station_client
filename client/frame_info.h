#pragma once

#include <iostream>
#include <memory>

namespace wmr928 {

   enum class type
   {
      unknown,
      wind,
      rain,
      thermo_hygro,
      mushroom,
      thermo_only,
      thermo_hygro_baro,
      minute,
      clock,
      thermo_hygro_baro2
   };

   namespace device {

      struct data
      {
     static type const frame_type { type::unknown };
     bool checksum { false };
      };

      struct minute
      {
     static type const frame_type { type::minute };
     int date_minute {};
     bool low_battery {};
     bool checksum { true };
      };

      struct clock
      {
     static type const frame_type { type::clock };
     int date_minute {};
     bool battery_low { false };
     unsigned char hour {};
     unsigned char day {};
     unsigned char month {};
     unsigned char year {};
     bool checksum { true };
      };

      struct wind
      {
     static type const frame_type { type::wind };
     bool gust_over { false };
     bool average_over { false };
     bool low_battery { false };
     int wind_direction {};
     float gust_wind_speed {};
     float average_wind_speed {};
     bool chill_no_data {};
     bool chill_over {};
     bool sign {};
     unsigned char wind_chill {};
     bool checksum { true };
      };

      struct rain
      {
     static type const frame_type { type::rain };
     bool rate_over { false };
     bool total_over { false };
     bool low_battery { false };
     bool yesterday_over { false };
     int rain_rate {};
     int total_rain_fall {};
     int yesterday_rainfall {};
     unsigned char start_date_minute {};
     unsigned char start_date_hour {};
     unsigned char start_date_day {};
     unsigned char start_date_month {};
     unsigned char start_date_year {};
     bool checksum { true };
      };

      struct thermo_hygro
      {
     static type const frame_type { type::thermo_hygro };
     char channel_number {};
     bool dew_under { false };
     bool low_battery { false };
     float temp {};
     unsigned char humidity {};
     unsigned char dew_temp {};
     bool checksum { true };
      };

      struct mushroom
      {
     static type const frame_type { type::mushroom };
     bool dew_under { false };
     bool low_battery { false };
     float temp {};
     unsigned char humidity {};
     unsigned char dew_temp {};
     bool checksum { true };
      };

      struct thermo_only
      {
     static type const frame_type { type::thermo_only };
     unsigned char channel_number {};
     bool low_battery { false };
     float temp {};
     bool checksum { true };
      };

      struct thermo_hygro_baro
      {
     static type const frame_type { type::thermo_hygro_baro };
     bool dew_under {};
     bool low_battery {};
     float temp {};
     unsigned char humidity {};
     unsigned char dew_temp {};
     unsigned char adc_baro_reading {};
     unsigned char weather_status {};
     float sea_level {};
     bool checksum { true };
      };

      struct thermo_hygro_baro2
      {
     static type const frame_type { type::thermo_hygro_baro2 };
     bool dew_under {};
     bool low_battery {};
     float temp {};
     unsigned char humidity {};
     unsigned char dew_temp {};
     unsigned char adc0_baro_reading {};
     bool ADCbit9 {};
     unsigned char weather_status {};
     float sea_level {};
     bool checksum { true };
      };
   }

   union data_union_type
   {
      device::rain r;
      device::wind w;
      device::thermo_hygro th;
      device::mushroom m;
      device::thermo_only to;
      device::thermo_hygro_baro thb;
      device::minute min ;
      device::clock c;
      device::thermo_hygro_baro2 thb2;
      device::data d;
   };

   struct data_union
   {
      data_union(type t, data_union_type u)
     : union_(u),
       frame_type{t}
      {}
      data_union(data_union const &other)
      {union_ = other.union_;
       frame_type = other.frame_type;}

   private:
      data_union_type union_{};

   public:
      device::rain *rain = &union_.r;
      device::wind *wind = &union_.w;
      device::thermo_hygro *thermo_hygro = &union_.th;
      device::minute *minute = &union_.min;
      device::mushroom *mushroom = &union_.m;
      device::thermo_only *thermo_only = &union_.to;
      device::thermo_hygro_baro *thermo_hygro_baro = &union_.thb;
      device::clock *clock = &union_.c;
      device::thermo_hygro_baro2 *thermo_hygro_baro2 = &union_.thb2;
      type frame_type;
      bool checksum {true};
   };

   struct frame_info
   {

      template<typename Container>
      static auto parse(type type, Container const& bitset) -> data_union
      {
     switch(type)
     {
     case type::wind :
        return data_union { type::wind, data_union_type{.w=parse_wind(bitset)} };
     case type::rain :
        return data_union { type::rain, data_union_type{.r=parse_rain(bitset)} };
     case type::thermo_hygro :
        return data_union { type::thermo_hygro, data_union_type{.th=parse_thermo_hygro(bitset) }};
     case type::mushroom :
        return data_union { type::mushroom, data_union_type{.m=parse_mushroom(bitset) }};
     case type::thermo_only :
        return data_union { type::thermo_only, data_union_type{.to=parse_thermo_only(bitset)} };
     case type::thermo_hygro_baro :
        return data_union { type::thermo_hygro_baro, data_union_type{.thb=parse_thermo_hygro_baro(bitset) }};
     case type::minute :
        return data_union { type::minute, data_union_type{.min=parse_minute(bitset)} };
     case type::clock :
        return data_union { type::clock, data_union_type{.c=parse_clock(bitset) }};
     case type::thermo_hygro_baro2 :
        return data_union { type::thermo_hygro_baro2, data_union_type{.thb2=parse_thermo_hygro_baro2(bitset) }};
     case type::unknown :
        return data_union { type::unknown, data_union_type{{}} };
     default:
        return data_union { type::unknown, data_union_type{{}} };
     }
      }

   private:
      template<typename Container>
      static auto parse_minute(Container const& bitset) -> device::minute
      {
     auto data = device::minute{};
     data.date_minute = ((bitset.at(3) & 0b01110000) >> 4) * 10 + (bitset.at(3) & 0b00001111);
     data.low_battery = (bitset.at(3) & 0b01000000);
     return data;
      }

      template<typename Container>
      static auto parse_wind(Container const& bitset) -> device::wind
      {
     auto data = device::wind {};

     data.gust_over = bitset.at(3) & 0b00010000;
     data.average_over = bitset.at(3) & 0b00100000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.wind_direction = (bitset.at(4) & 0x0F) + ((bitset.at(4) & 0xF0) >> 4 )* 10 + (bitset.at(5) & 0x0F) * 100;
     data.gust_wind_speed = ((bitset.at(5) & 0xF0) >> 4) * 0.1f + (bitset.at(6) & 0x0F) + ((bitset.at(6) & 0xF0) >> 4) * 10;
     data.average_wind_speed = ((bitset.at(7) & 0x0F) >> 4) * 0.1f + ((bitset.at(7) & 0xF0) >> 4) + (bitset.at(8) & 0x0F) * 10;
     data.chill_no_data = bitset.at(8) & 0b00010000;
     data.chill_over = bitset.at(8) & 0b01000000;
     data.sign = bitset.at(8) & 0b10000000;
     data.wind_chill = (bitset.at(9) & 0x0F) + ((bitset.at(9) & 0xF0) >> 4) * 10;

     return data;
      };

      template<typename Container>
      static auto parse_clock(Container const& bitset) -> device::clock
      {
     auto data = device::clock {};

     data.date_minute = (bitset.at(3) & 0x01110000) * 10 + (bitset.at(3) & 0x0F);
     data.battery_low = bitset.at(3) & 0b10000000;
     data.hour = (bitset.at(4) & 0x0F) + ((bitset.at(4) & 0xF0) >> 4) * 10;
     data.day = (bitset.at(5) & 0x0F) + ((bitset.at(5) & 0xF0) >> 4) * 10;
     data.month = (bitset.at(6) & 0x0F) + ((bitset.at(5) & 0xF0) >> 4) * 10;
     data.year = (bitset.at(7) & 0x0F) + ((bitset.at(5) & 0xF0) >> 4) * 10;
     return data;
      }

      template<typename Container>
      static auto parse_rain(Container const& bitset) -> device::rain
      {
     auto data = device::rain {};

     data.rain_rate = bitset.at(3) & 0b00010000;
     data.total_over = bitset.at(3) & 0b00100000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.yesterday_over = bitset.at(3) & 0b10000000;
     data.rain_rate = (bitset.at(4) & 0x0F) + ((bitset.at(4) & 0xF0) >> 4) * 10 + (bitset.at(5) & 0x0F) * 100;
     data.total_rain_fall = ((bitset.at(5) & 0xF0) >> 4) * 0.1 + (bitset.at(6) & 0x0F)
        + ((bitset.at(6) & 0xF0) >> 4) * 10 + (bitset.at(7) & 0x0F) * 100 + ((bitset.at(7) * 0xF0) >> 4) * 1000;
     data.yesterday_rainfall = (bitset.at(8) & 0x0F) + ((bitset.at(8) & 0xF0) >> 4) * 10
        + (bitset.at(9) & 0x0F) * 100 + ((bitset.at(9) & 0xF0) >> 4) * 1000;
     data.start_date_minute = (bitset.at(10) & 0x0F) + ((bitset.at(10) & 0xF0) >> 4) * 10;
     data.start_date_hour = (bitset.at(11) & 0x0F) + ((bitset.at(11) & 0xF0) >> 4) * 10;
     data.start_date_day =  (bitset.at(12) & 0x0F) + ((bitset.at(12) & 0xF0) >> 4) * 10;
     data.start_date_month =  (bitset.at(13) & 0x0F) + ((bitset.at(13) & 0xF0) >> 4) * 10;
     data.start_date_year =  (bitset.at(14) & 0x0F) + ((bitset.at(14) & 0xF0) >> 4) * 10;

     return data;
      }

      template<typename Container>
      static auto parse_thermo_hygro(Container const& bitset) -> device::thermo_hygro
      {
     auto data = device::thermo_hygro {};

     data.channel_number = bitset.at(3) & 0x0F;
     if(data.channel_number == 4)
        data.channel_number = 3;
     data.dew_under = bitset.at(3) & 0b00010000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.temp = (bitset.at(4) & 0x0F) * 0.1 + ((bitset.at(4) & 0xF0) >> 4) + (bitset.at(5) & 0x0F) * 10
        + ((bitset.at(5) & 0b00110000) >> 4) * 100;
     if(bitset.at(5) & 0b10000000)
        data.temp *= -1;
     data.humidity = (bitset.at(6) & 0x0F) + ((bitset.at(6) & 0xF0) >> 4) * 10;
     data.dew_temp = (bitset.at(7) & 0x0F) + ((bitset.at(7) & 0xF0) >> 4) * 10;

     return data;
      }

      template<typename Container>
      static auto parse_mushroom(Container const& bitset) -> device::mushroom
      {
     auto data = device::mushroom {};

     data.dew_under = bitset.at(3) & 0b00010000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.temp = (bitset.at(4) & 0x0F) * 0.1 + ((bitset.at(4) & 0xF0) >> 4) + (bitset.at(5) & 0x0F) * 10 + ((bitset.at(5) & 0b00110000) >> 4) * 100;
     if(bitset.at(5) & 0b10000000)
        data.temp *= -1;
     data.humidity = (bitset.at(6) & 0x0F) + ((bitset.at(6) & 0xF0) >> 4) * 10;
     data.dew_temp = (bitset.at(7) & 0x0F) + ((bitset.at(7) & 0xF0) >> 4) * 10;
     return data;
      }

      template<typename Container>
      static auto parse_thermo_only(Container const& bitset) -> device::thermo_only
      {
     auto data = device::thermo_only {};

     data.channel_number = bitset.at(3) & 0x0F;
     if(data.channel_number == 4)
        data.channel_number = 3;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.temp = (bitset.at(4) & 0x0F) * 0.1 + ((bitset.at(4) & 0xF0) >> 4) + (bitset.at(5) & 0x0F) * 10 + (bitset.at(5) & 0b00110000) * 100;
     if(bitset.at(5) & 0b10000000)
        data.temp *= -1;

     return data;
      }

      template<typename Container>
      static auto parse_thermo_hygro_baro(Container const& bitset) -> device::thermo_hygro_baro
      {
     auto data = device::thermo_hygro_baro {};

     data.dew_under = bitset.at(3) & 0b00010000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.temp = (bitset.at(4) & 0x0F) * 0.1 + ((bitset.at(4) & 0xF0) >> 4)
        + (bitset.at(5) & 0x0F) * 10 + ((bitset.at(5) & 0b00110000) >> 4) * 100;
     if(bitset.at(5) & 0b10000000)
        data.temp *= -1;
     data.humidity = (bitset.at(6) & 0x0F) + ((bitset.at(6) & 0xF0) >> 4) * 10;
     data.dew_temp = (bitset.at(7) & 0x0F) + ((bitset.at(7) & 0xF0) >> 4) * 10;
     data.adc_baro_reading = bitset.at(8);
     data.weather_status = bitset.at(9) & 0x0F;
     data.sea_level = (bitset.at(10) & 0x0F) * 0.01 + ((bitset.at(10) & 0xF0) >> 4)
        + (bitset.at(11) & 0x0F) * 10 + ((bitset.at(11) & 0xF0) >> 4) * 100;

     return data;
      }

      template<typename Container>
      static auto parse_thermo_hygro_baro2(Container const& bitset) -> device::thermo_hygro_baro2
      {
     auto data = device::thermo_hygro_baro2 {};

     data.dew_under = bitset.at(3) & 0b00010000;
     data.low_battery = bitset.at(3) & 0b01000000;
     data.temp = (bitset.at(4) & 0x0F) * 0.1 + ((bitset.at(4) & 0xF0) >> 4)
        + (bitset.at(5) & 0x0F) * 10 + ((bitset.at(5) & 0b00110000) >> 4) * 100;
     if((bitset.at(5) & 0b10000000) == 0b10000000)
        data.temp *= -1;
     data.humidity = (bitset.at(6) & 0x0F) + ((bitset.at(6) & 0xF0) >> 4) * 10;
     data.dew_temp = (bitset.at(7) & 0x0F) + ((bitset.at(7) & 0xF0) >> 4) * 10;
     data.adc0_baro_reading = bitset.at(8);
     data.ADCbit9 = bitset.at(9) & 0b00000001;
     data.sea_level = ((bitset.at(10) & 0xF0) >> 4) * 0.1 + (bitset.at(11) & 0x0F)
        + ((bitset.at(11) & 0xF0) >> 4) * 10 + (bitset.at(12) & 0x0F) * 100 + ((bitset.at(12) & 0xF0) >> 4) * 1000;

     return data;
      }
   };
};


