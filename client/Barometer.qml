import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#fb0000"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                color: "#000000"
                text: qsTr("Pression")
                font.pointSize: 14
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
            }

        }
        CircularGauge {
            id: circularGauge
            width: parent.width / 2
            height: parent.height
            minimumValue: 600
            maximumValue: 1300
            value: pressure
            anchors {
                top: parent.top
                topMargin: parent.height * 0.25
                left: parent.left
                leftMargin: parent.width * 0.05
            }

            style: CircularGaugeStyle {
                labelStepSize: 100
                minimumValueAngle: -90
                maximumValueAngle: 90
                tickmarkStepSize: 50
                needle: Rectangle {
                    y: outerRadius * 0.15
                    implicitWidth: outerRadius * 0.03
                    implicitHeight: outerRadius * 0.9
                    antialiasing: true
                    color: "#000000"

                }
            }
        }

        Label {
            id: labelPressionValue
            color: "#ffffff"
            text: pressure
            font.pointSize: 16
            anchors {
                top: parent.top
                topMargin: parent.height * 0.15
                left: parent.left
                leftMargin: parent.width * 0.65
            }
        }

        Label {
            id: labelPressureUnit
            color: "#ffffff"
            text: qsTr("mBar")
            font.pointSize: 14
            anchors {
                top: labelPressionValue.bottom
                left: parent.left
                leftMargin: parent.width * 0.65
            }
        }
    }
}
