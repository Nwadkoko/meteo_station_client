import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls 1.4
import QtQuick.Extras 1.4

Window {
    id: window
    visible: true
    width: 1280
    height: 920
    color: "#444444" //"#f9f9f9"
    title: qsTr("Client")

    property real arrow_angle: 0
    property real inside_temperature: 0
    property real inside_humidity: 0
    property real inside_dew_temp: 0
    property real outside_temperature: 0
    property real outside_humidity: 0
    property real outside_dew_temp: 0
    property real wind_speed_gust: 0
    property real wind_speed_average: 0
    property real wind_chill: 0
    property real rain_level: 0
    property real dew_point: 0
    property real total_rainfall: 0
    property real yesterday_rainfall: 0
    property real pressure: 0
    property real pressure1: 0
    property real pressure3: 0
    property real pressure6: 0
    property real pressure12: 0
    property real pressure24: 0
    property real valueProgressBar: 0
    property bool wind_is_low: false
    property bool rain_is_low: false
    property bool mushroom_is_low: false
    property bool tHB2_is_low: false
    property string ip_address: "0.0.0.0"

    Connections {
        target: dataHandler

        onCurrentDataChanged: {
            inside_temperature  =   dataHandler.current_data.inside_temperature.toFixed(2);
            inside_humidity     =   dataHandler.current_data.inside_humidity.toFixed(2);
            inside_dew_temp     =   dataHandler.current_data.inside_dew_temp.toFixed(2);
            outside_temperature =   dataHandler.current_data.outside_temperature.toFixed(2);
            outside_humidity    =   dataHandler.current_data.outside_humidity.toFixed(2);
            outside_dew_temp    =   dataHandler.current_data.outside_dew_temp.toFixed(2);
            arrow_angle         =   dataHandler.current_data.wind_direction;
            rain_level          =   dataHandler.current_data.rain;
            wind_speed_gust     =   dataHandler.current_data.wind_speed_gust;
            wind_speed_average  =   dataHandler.current_data.wind_speed_average;
            wind_chill          =   dataHandler.current_data.wind_chill;
            total_rainfall      =   dataHandler.current_data.total_rainfall;
            yesterday_rainfall  =   dataHandler.current_data.yesterday_rainfall;
            pressure            =   dataHandler.current_data.pressure;
            wind_is_low         =   dataHandler.current_data.windLowBatt;
            rain_is_low         =   dataHandler.current_data.rainLowBatt;
            mushroom_is_low     =   dataHandler.current_data.mushroomLowBatt;
            tHB2_is_low         =   dataHandler.current_data.thermoHygroBaro2LowBatt;

            if(valueProgressBar >= 1)
            {
                valueProgressBar = 0;
            }
            else
            {
                valueProgressBar += 0.1;
            }

            console.log(valueProgressBar);
        }
    }

    ConnectionMenu {
        id: m_connection
        anchors {
            centerIn: parent
        }
    }

    OutsideTemperature {
        id: m_outsidetemp
        anchors {
            left: parent.left
            leftMargin: 10
            top: parent.top
            topMargin: 10
        }
        visible: false
    }

    InsideTemperature {
        id: m_insidetemp
        anchors {
            left: m_outsidetemp.right
            leftMargin: 20
            top: parent.top
            topMargin: 10
        }
        visible: false
    }

    ConfigurationTemp {
        id: m_configurationtemp
        anchors {
            left: m_insidetemp.right
            leftMargin: 20
            top: parent.top
            topMargin: 10
        }
        visible: false
    }

    Wind {
        id: m_wind
        anchors {
            left: parent.left
            leftMargin: 10
            top: m_outsidetemp.bottom
            topMargin: 10
        }
        visible: false
    }

    Rain {
        id: m_rain
        anchors {
            left: m_wind.right
            leftMargin: 20
            top: m_insidetemp.bottom
            topMargin: 10
        }
        visible: false
    }

    ConfigurationWindRain {
        id: m_configurationwindrain
        anchors {
            left: m_rain.right
            leftMargin: 20
            top: m_configurationtemp.bottom
            topMargin: 10
        }
        visible: false
    }

    Barometer {
        id: m_barometer
        anchors {
            left: parent.left
            leftMargin: 10
            top: m_wind.bottom
            topMargin: 10
        }
        visible: false
    }

    ConfigurationAlarm {
        id: m_configurationalarm
        x: 870
        anchors {
            top: m_configurationwindrain.bottom
            topMargin: 10
        }
        visible: false
    }

    BarStatus {
        id: m_barstatus
        width: 401
        anchors.bottomMargin: 200
        anchors {
            left: m_barometer.right
            leftMargin: 20
            top: m_rain.bottom
            topMargin: 10
        }
        visible: false
    }
}
