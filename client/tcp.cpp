#include "tcp.h"
#include "wmr928.h"

TCP::TCP()
{

}

void TCP::sendFrame(QString t)
{
    QTextStream frame(&soc);
    frame << t << endl;
}

bool TCP::isConnectOk()
{
    return this->connected;
}

QString TCP::getIpAddress()
{
    return this->IP;
}

void TCP::setIP(QString _port, QString _IP)
{
    this->port = _port.toUShort();
    this->IP = _IP;
    qDebug() << "ip " << this->IP;
    qDebug() << "port : " << this->port;
    soc.connectToHost(this->IP,this->port);
    QObject::connect(&soc,SIGNAL(connected()),this,SLOT(isConnected()));
    QObject::connect(&soc, SIGNAL(disconnected()), this, SLOT(isDisconnected()));
    QObject::connect(&soc, SIGNAL(readyRead()), this, SLOT(read()));
}

void TCP::isConnected()
{
    this->connected = true;
    emit connect();
}

void TCP::isDisconnected()
{
    this->connected = false;
    emit connect();
}

void TCP::read()
{
    QByteArray arr;
    if(soc.bytesAvailable())
    {
        soc.setReadBufferSize(8);
        arr.append(soc.readAll());

        std::vector<unsigned char> vec(arr.begin(), arr.end());

        qDebug() << "vector : " << vec;

        emit send(vec);
    }
}
