import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        width: 400
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#00aef2"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelConfigurationWindRainModule
                color: "#000000"
                text: qsTr("Configuration alarme")
                font.pointSize: 14
                anchors {
                    centerIn: parent
                }
            }
        }

        Label {
            id: labelConfigVitesseMax
            text: qsTr("Max. vitesse vent :")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: parent.top
                topMargin: 35
            }
        }

        DoubleSpinBox {
            id: valueConfigVitesseMax
            from: 0
            to: 100
            stepSize: 0.5
            anchors {
                left: labelConfigVitesseMax.right
                leftMargin: 5
                verticalCenter: labelConfigVitesseMax.verticalCenter
            }
        }

        Label {
            id: labelConfigWindAngle
            text: qsTr("Direction du vent :")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigVitesseMax.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigWindAngleBottom
            from: 0
            to: 359
            stepSize: 1
            anchors {
                left: labelConfigWindAngle.right
                leftMargin: 5
                verticalCenter: labelConfigWindAngle.verticalCenter
            }
        }

        DoubleSpinBox {
            id: valueConfigWindAngleTop
            from: valueConfigWindAngleBottom.value
            to: 359
            stepSize: 1
            anchors {
                left: valueConfigWindAngleBottom.right
                leftMargin: 5
                verticalCenter: valueConfigWindAngleBottom.verticalCenter
            }

        }

        Label {
            id: labelConfigRainToday
            text: qsTr("Total précipitations :")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigWindAngle.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigRainToday
            from: 0
            to: 50
            stepSize: 0.5
            anchors {
                left: labelConfigRainToday.right
                leftMargin: 5
                verticalCenter: labelConfigRainToday.verticalCenter
            }
        }

        Label {
            id: labelConfigInstantRain
            text: qsTr("Précipitations :")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigRainToday.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigInstantRain
            from: 0
            to: 50
            stepSize: 0.5
            anchors {
                left: labelConfigInstantRain.right
                leftMargin: 5
                verticalCenter: labelConfigInstantRain.verticalCenter
            }
        }

        Label {
            id: labelAlarmRain
            text: "Pluie"
            font.pointSize: 14
            anchors {
                horizontalCenter: switchAlarmRain.horizontalCenter
                bottom: switchAlarmRain.top
                bottomMargin: 5
            }
        }

        Switch {
            id: switchAlarmRain
            checked: true
            anchors {
                horizontalCenter: sendButton.horizontalCenter
                bottom: labelAlarmWind.top
                bottomMargin: 5
            }
            onToggled: {
                if(switchAlarmRain.checked)
                {
                    sendButton.sendRainConfig();
                }
                else
                {
                    dataHandler.sendConfig(1, 1);
                    dataHandler.sendConfig(1, 2);
                }
            }
        }

        Label {
            id: labelAlarmWind
            text: "Vents"
            font.pointSize: 14
            anchors {
                horizontalCenter: switchAlarmWind.horizontalCenter
                bottom: switchAlarmWind.top
                bottomMargin: 5
            }
        }

        Switch {
            id: switchAlarmWind
            checked: true
            anchors {
                horizontalCenter: sendButton.horizontalCenter
                bottom: sendButton.top
                bottomMargin: 5
            }
            onToggled: {
                if(switchAlarmWind.checked)
                {
                    sendButton.sendWindConfig();
                }
                else
                {
                    dataHandler.sendConfig(0, 1);
                    dataHandler.sendConfig(0, 4);
                }
            }
        }

        Button {
            id: sendButton
            text: "Envoyer"
            anchors {
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 90
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: 155
            }
            onClicked: {
                sendWindRainConfig();
            }

            function sendWindRainConfig() {
                if(switchAlarmRain.checked || switchAlarmWind.checked){
                    if(valueConfigInstantRain.value !== 0)
                    {
                        dataHandler.sendConfig(1, 1, valueConfigInstantRain.value);
                    }
                    if(valueConfigRainToday.value !== 0)
                    {
                        dataHandler.sendConfig(1, 2, valueConfigRainToday.value);
                    }
                    if(valueConfigVitesseMax.value !== 0)
                    {
                        dataHandler.sendConfig(0, 1, valueConfigVitesseMax.value);
                    }
                    if(valueConfigWindAngleBottom.value !== 0 && valueConfigWindAngleTop.value >= valueConfigWindAngleBottom.value)
                    {
                        dataHandler.sendConfig(0, 4, valueConfigWindAngleBottom.value, valueConfigWindAngleTop.value);
                    }
                }
            }

            function sendWindConfig() {
                if(switchAlarmWind.checked)
                {
                    if(valueConfigVitesseMax.value !== 0)
                    {
                        dataHandler.sendConfig(0, 1, valueConfigVitesseMax.value);
                    }
                    if(valueConfigWindAngleBottom.value !== 0 && valueConfigWindAngleTop.value >= valueConfigWindAngleBottom.value)
                    {
                        dataHandler.sendConfig(0, 4, valueConfigWindAngleBottom.value, valueConfigWindAngleTop.value);
                    }
                }
            }

            function sendRainConfig() {
                if(switchAlarmRain.checked)
                {
                    if(valueConfigInstantRain.value !== 0)
                    {
                        dataHandler.sendConfig(1, 1, valueConfigInstantRain.value);
                    }
                    if(valueConfigRainToday.value !== 0)
                    {
                        dataHandler.sendConfig(1, 2, valueConfigRainToday.value);
                    }
                }
            }
        }
    }
}
