import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangle1
            width: 120
            height: 120
            color: "#ffae2e"
            radius: 100
            anchors {
                left: parent.left
                leftMargin: 10
                verticalCenter: parent.verticalCenter
            }
        }

        Rectangle {
            id: rectangle2
            color: "#494949"
            width: 100
            height: 100
            radius: 100
            anchors {
                left: rectangle1.left
                leftMargin: 10
                top: rectangle1.top
                topMargin: 10
            }

        }

        Rectangle {
            id: rectangleTitle
            color: "#ffae2e"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                color: "#000000"
                text: qsTr("Temperature intérieure")
                font.pointSize: 14
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
            }
        }

        Label {
            id: labelTemperatureInValue
            color: "#ffffff"
            text: inside_temperature + qsTr("°C")
            font.pointSize: 20
            anchors {
                verticalCenter: parent.verticalCenter
                left: rectangle1.left
                leftMargin: 20
            }
        }

        Label {
            id: labelHumidity
            color: "#ffae2e"
            text: qsTr("Humidité")
            font.pointSize: 14
            anchors {
                top: parent.top
                topMargin: parent.height * 0.15
                left: parent.left
                leftMargin: parent.width * 0.50
            }
        }

        Label {
            id: labelHumidityValue
            color: "#ffffff"
            text: inside_humidity + qsTr(" %")
            font.pointSize: 14
            anchors {
                top: labelHumidity.bottom
                left: parent.left
                leftMargin: parent.width * 0.50
            }
        }

        Label {
            id: labelDewTemp
            color: "#ffae2e"
            text: qsTr("Point de rosée")
            font.pointSize: 14
            anchors {
                top: labelHumidityValue.bottom
                topMargin: 10
                left: parent.left
                leftMargin: parent.width * 0.50
            }
        }

        Label {
            id: labelDewTempValue
            color: "#ffffff"
            text: inside_dew_temp + qsTr(" °C")
            font.pointSize: 14
            anchors {
                top: labelDewTemp.bottom
                left: parent.left
                leftMargin: parent.width * 0.50
            }
        }
    }
}
