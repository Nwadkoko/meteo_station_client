import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        width: 400
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#ffae2e"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                x: 104
                y: 0
                color: "#000000"
                text: qsTr("Configuration alarme")
                font.pointSize: 14
            }
        }

        Label {
            id: labelConfigTemperatureInMin
            text: qsTr("Temp. intérieure minimale:")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: parent.top
                topMargin: 35
            }
        }

        DoubleSpinBox {
            id: valueConfigTemperatureInMin
            from: -50
            to: 50
            stepSize: 0.50
            anchors {
                left: labelConfigTemperatureInMin.right
                leftMargin: 5
                verticalCenter: labelConfigTemperatureInMin.verticalCenter
            }
        }

        Label {
            id: labelConfigTemperatureInMax
            text: qsTr("Temp. intérieure maximale:")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigTemperatureInMin.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigTemperatureInMax
            from: -50
            to: 50
            stepSize: 0.50
            anchors {
                left: labelConfigTemperatureInMax.right
                leftMargin: 5
                verticalCenter: labelConfigTemperatureInMax.verticalCenter
            }
        }

        Label {
            id: labelConfigTemperatureOutMin
            text: qsTr("Temp. extérieure minimale:")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigTemperatureInMax.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigTemperatureOutMin
            from: -50
            to: 50
            stepSize: 0.50
            anchors {
                left: labelConfigTemperatureOutMin.right
                leftMargin: 5
                verticalCenter: labelConfigTemperatureOutMin.verticalCenter
            }
        }

        Label {
            id: labelConfigTemperatureOutMax
            text: qsTr("Temp. extérieure maximale:")
            color: "#ffffff"
            font.pointSize: 14
            anchors {
                left: parent.left
                leftMargin: 10
                top: labelConfigTemperatureOutMin.bottom
                topMargin: 30
            }
        }

        DoubleSpinBox {
            id: valueConfigTemperatureOutMax
            from: -50
            to: 50
            stepSize: 0.50
            anchors {
                left: labelConfigTemperatureOutMax.right
                leftMargin: 5
                verticalCenter: labelConfigTemperatureOutMax.verticalCenter
            }
        }

        Label {
            id: labelAlarmTempIn
            text: "Alarme"
            font.pointSize: 14
            anchors {
                horizontalCenter: switchAlarmTemp.horizontalCenter
                bottom: switchAlarmTemp.top
                bottomMargin: 5
            }
        }

        Switch {
            id: switchAlarmTemp
            checked: true
            anchors {
                horizontalCenter: sendButton.horizontalCenter
                bottom: sendButton.top
                bottomMargin: 5
            }
            onToggled: {
                if(switchAlarmTemp.checked)
                {
                    sendButton.sendTemperatureConfig();
                }
                else
                {
                    dataHandler.sendConfig(5, 1);
                }
            }
        }

        Button {
            id: sendButton
            text: "Envoyer"
            anchors {
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 90
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: 155
            }
            onClicked: {
                sendTemperatureConfig();
            }

            function sendTemperatureConfig() {
                if((valueConfigTemperatureInMin.value || valueConfigTemperatureInMax.value) !== 0)
                {
                    dataHandler.sendConfig(5, 1, valueConfigTemperatureInMin.value, valueConfigTemperatureInMax.value);
                }
                if((valueConfigTemperatureOutMax.value || valueConfigTemperatureOutMin.value) !== 0)
                {
                    dataHandler.sendConfig(2, 1, valueConfigTemperatureOutMin.value, valueConfigTemperatureOutMax.value);
                }
            }
        }
    }
}
