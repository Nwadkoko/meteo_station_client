import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQml 2.3

Item {
    id : clock
    width: 250
    height: 100

    Timer {
        interval: 500; running: true; repeat: true
        onTriggered: {
            labelTime.text = new Date().toLocaleTimeString(Qt.locale("fr_FR"), "hh:mm:ss")
            labelDate.text = new Date().toLocaleDateString(Qt.locale("fr_FR"), "dd/MM/yy")
        }
    }

    Label {
        id: labelTime
        width: parent.width / 2
        height: parent.height
        color: "#ffffff"
        font.family: "Roboto"
        text: qsTr("00:00:00")
        font.pointSize: 100
        minimumPointSize: 10
        fontSizeMode: Text.Fit
    }

    Label {
        id: labelDate
        anchors.top: labelTime.bottom
        width: parent.width / 2
        height: parent.height
        color: "#ffffff"
        font.family: "Roboto"
        text: qsTr("01/01/99")
        font.pointSize: 100
        minimumPointSize: 10
        fontSizeMode: Text.fit
        anchors {
            right: labelTime.left
            rightMargin: 5
            verticalCenter: labelTime.verticalCenter
        }
    }
}
