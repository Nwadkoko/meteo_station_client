#pragma once

#include <type_traits>
#include <vector>
#include <numeric>
#include <algorithm>

#include "frame_info.h"

namespace wmr928
{
   enum class parameter { erase_parsed_data, keep_parsed_data };

   template<typename Container>
   auto parse(Container const& args) -> data_union;

   template<typename Container>
   auto frame_type(Container const& bitsets) -> type;

   template<typename Container>
   auto check_size(Container const& bitsets) -> bool;

   template<typename Container>
   auto checksum(Container const& bitsets) -> bool;

   template<typename Container>
   auto has_unusable_data(Container const& data) -> bool;

   template<typename T>
   auto remove_unusable_data(std::vector<T>& data) -> void;

   template<typename Container>
   auto has_enough_data(Container& data) -> bool;

   template<typename Container>
   auto end_of(Container& data) -> typename Container::iterator;

   template<typename Container>
   auto parse_buffer(Container& data, parameter param = parameter::erase_parsed_data) -> data_union;

   std::vector<unsigned char> const signature{255, 255};

   template<typename Container>
   auto parse(Container const& args) -> data_union
   {
      static_assert(std::is_same<typename Container::value_type, unsigned char>::value, "Type should be unsigned char.");

      auto type = frame_type(args);

      if(type == type::unknown or !checksum(args))
      {
     return data_union { type::unknown, {{}} };
      }

      return frame_info::parse<Container>(type, args);
   }

   template<typename Container>
   auto frame_type(Container const& bitsets) -> type
   {
      if(bitsets.size() < 5 or bitsets.at(2) > 0b0000'1111)
      {
     return type::unknown;
      }

      switch(bitsets[2]) {
      case 0b0000'0000: return type::wind;
      case 0b0000'0001: return type::rain;
      case 0b0000'0010: return type::thermo_hygro;
      case 0b0000'0011: return type::mushroom;
      case 0b0000'0100: return type::thermo_only;
      case 0b0000'0101: return type::thermo_hygro_baro;
      case 0b0000'0110: return type::thermo_hygro_baro2;
      case 0b0000'1110: return type::minute;
      case 0b0000'1111: return type::clock;
      default: return type::unknown;
      }
   }

   template<typename Container>
   auto check_size(Container const& bitsets) -> bool
   {
      switch(frame_type(bitsets))
      {
      case type::wind : return bitsets.size() == 11;
      case type::rain : return bitsets.size() == 16;
      case type::thermo_hygro : return bitsets.size() == 9;
      case type::mushroom : return bitsets.size() == 9;
      case type::thermo_only : return bitsets.size() == 7;
      case type::thermo_hygro_baro : return bitsets.size() == 13;
      case type::minute : return bitsets.size() == 5;
      case type::clock : return bitsets.size() == 9;
      case type::thermo_hygro_baro2 : return bitsets.size() == 14;
      case type::unknown : return false;
      default: return false;
      }
   }

   template<typename Container>
   auto checksum(Container const& data) -> bool
   {
      auto sum = std::accumulate(data.begin(), data.end() - 1, 0);

      return data.back() == static_cast<unsigned char>(sum);
   }

   template<typename Container>
   auto has_unusable_data(Container const& data) -> bool
   {
      if(data.at(0) == 0b1111'1111 and data.at(1) == 0b1111'1111)
     return false;
      return true;
   }

   template<typename T>
   auto remove_unusable_data(std::vector<T>& data) -> void
   {
      auto pos = std::search(data.begin(), data.end(), signature.begin(), signature.end());

      data.erase(data.begin(), pos);
   }

   template<typename Container>
   auto has_enough_data(Container& data) -> bool
   {
      if(data.size() < 7)
      {
     return false;
      }
      if(has_unusable_data(data))
      {
     remove_unusable_data(data);
      }
      return !(std::search(data.begin(), data.end(), signature.begin(), signature.end()) == data.end());
   }

   template<typename Container>
   auto end_of(Container& data) -> typename Container::iterator
   {
      if(!has_enough_data(data))
     return data.begin();
      return std::search(data.begin() + 2, data.end(), signature.begin(), signature.end());
   }

   template<typename Container>
   auto parse_buffer(Container& data, parameter param) -> data_union
   {
      auto it = end_of(data);
      if(it == data.begin())
     return data_union { type::unknown, {{}} };

      auto tmp = parse(Container(data.begin(), it));

      if(param == parameter::erase_parsed_data and has_enough_data(data))
     data.erase(data.begin(), it);

      return tmp;
   }
}
