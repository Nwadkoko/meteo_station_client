import QtQuick 2.0
import QtCharts 2.2

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        ChartView {
            backgroundColor: "#494949"
            plotAreaColor: "#b9b9b9"
            title: "Variation pression"
            anchors.fill: parent
            height: parent.height
            width: parent.width
            legend.alignment: Qt.AlignBottom
            legend.color: "#ffffff"

            ValueAxis {
                id: yAxis
                min: -6
                max: 4
                tickCount: 6
                color: "#ffffff"
            }        

            BarSeries {
                id: mySeries
                axisX: BarCategoryAxis {categories: ["-24", "-12", "-6", "-3", "-1", "0" ]}
                       BarSet { values: [pressure24, pressure12, pressure6, pressure3, pressure1, 0]; color: "#00A3E4"; }
                axisY: yAxis
            }
        }
    }
}
