#pragma once

#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QDebug>
#include <vector>
#include <QBitArray>
#include <QProcess>

class TCP : public QObject
{
    Q_OBJECT
public:
    TCP();
public slots:
    void sendFrame(QString t);
    bool isConnectOk();
    QString getIpAddress();
    void setIP(QString _port, QString _IP);
private slots:
    void isConnected();
    void isDisconnected();
    void read();
signals:
    void connect();
    void send(std::vector<unsigned char>);
private:
    QString IP;
    quint16 port;
    QTcpSocket soc;
    bool connected;
};
