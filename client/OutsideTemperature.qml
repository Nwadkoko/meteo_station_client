import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangle1
            x: 8
            y: 4
            width: 120
            height: 120
            color: "#ffae2e"
            radius: 100
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 10
            }
        }

        Rectangle {
            id: rectangle2
            color: "#494949"
            width: 100
            height: 100
            radius: 100
            anchors {
                left: rectangle1.left
                leftMargin: 10
                top: rectangle1.top
                topMargin: 10
            }

        }

        Rectangle {
            id: rectangleTitle
            color: "#ffae2e"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                color: "#000000"
                text: qsTr("Temperature extérieure")
                font.pointSize: 14
                anchors {
                    centerIn: parent
                }
            }
        }

        Label {
            id: labelDewTemp
            x: 202
            y: 41
            color: "#ffae2e"
            text: qsTr("Point de rosée")
            anchors.verticalCenterOffset: -20
            font.pointSize: 14
            anchors.leftMargin: 0
        }

        Label {
            id: labelDewTempValue
            x: 202
            width: 300
            height: 23
            color: "#ffffff"
            text: outside_dew_temp + qsTr(" °C")
            anchors.top: labelDewTemp.bottom
            anchors.topMargin: -1
            font.pointSize: 14
        }

        Label {
            id: labelTemperatureOutValue
            x: 19
            y: 45
            width: 134
            height: 111
            color: "#ffffff"
            text: outside_temperature + qsTr("°C")
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: rectangle1.left
            anchors.leftMargin: 20
            font.pointSize: 20
        }

        Label {
            id: labelHumidity
            x: 202
            y: 98
            color: "#ffae2e"
            text: qsTr("Humidité")
            font.pointSize: 14
            anchors.verticalCenterOffset: -20
            anchors.leftMargin: 0
        }

        Label {
            id: labelHumidityValue
            x: 202
            y: 118
            width: 300
            height: 23
            color: "#ffffff"
            text: outside_humidity + qsTr(" %")
            font.pointSize: 14
            anchors.topMargin: -1
            anchors.top: labelHumidity.bottom
        }

        Label {
            id: labelWindChill
            x: 202
            y: 149
            color: "#ffae2e"
            text: qsTr("Ressenti")
            font.pointSize: 14
            anchors.leftMargin: 0
        }

        Label {
            id: labelWindChillValue
            x: 202
            y: 172
            width: 300
            height: 23
            color: "#ffffff"
            text: wind_chill + qsTr(" °C")
            anchors.topMargin: -3
            font.pointSize: 14
            anchors.top: labelWindChill.bottom
        }
    }
}
