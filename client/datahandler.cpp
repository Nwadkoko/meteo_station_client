#include "datahandler.h"
#include "wmr928.h"

DataHandler::DataHandler()
{
    /*file = new QFile("pressure.csv");
    QString line;
    if(file->open(QFile::ReadOnly))
    {
        while(!file->atEnd())
        {
            line = file->readLine();
        }

        if(line.split('\t').size() > 1)
        {
            if((QDateTime::currentDateTime().toSecsSinceEpoch() - QDateTime::fromString(line.split('\t')[0]).toSecsSinceEpoch()) > 86400)
            {
                file->remove();
                file = new QFile("pressure.csv");
            }
        }
    }*/

    this->current_data.m_inside_temperature = settings.value("thermo_hygro_baro2/inside_temperature").toFloat();
    this->current_data.m_inside_humidity = settings.value("thermo_hygro_baro2/inside_humidity").toFloat();
    this->current_data.m_inside_dew_temp = settings.value("thermo_hygro_baro2/inside_dew_temp").toFloat();
    this->current_data.m_outside_temperature = settings.value("mushroom/outside_temperature").toFloat();
    this->current_data.m_outside_humidity = settings.value("mushroom/outside_humidity").toFloat();
    this->current_data.m_outside_dew_temp = settings.value("mushroom/outside_dew_temp").toFloat();
    this->current_data.m_wind_chill = settings.value("wind/wind_chill").toFloat();
    this->current_data.m_wind_direction = settings.value("wind/wind_direction").toFloat();
    this->current_data.m_pressure = settings.value("thermo_hygro_baro2/pressure").toFloat();

    //QObject::connect(&TCP_Client, SIGNAL(send(std::vector<unsigned char>)), this, SLOT(getFrame(std::vector<unsigned char>)));
}

void DataHandler::writeSettings(QVariant value)
{
    settings.setValue("test", value);
    settings.sync();
}

QVariant DataHandler::readSettings()
{
    return settings.value("test");
}

void DataHandler::getFrame(std::vector<unsigned char> frame)
{
    buffer.insert(buffer.end(), frame.begin(), frame.end());
    const auto data = wmr928::parse_buffer(buffer);

    switch(data.frame_type){
    case wmr928::type::minute:
        break;

    case wmr928::type::rain:
        this->current_data.m_rain = data.rain->rain_rate;
            settings.setValue("rain/rain", current_data.m_rain);
        this->current_data.m_total_rainfall = data.rain->total_rain_fall;
            settings.setValue("rain/total_rainfall", current_data.m_total_rainfall);
        this->current_data.m_yesterday_rainfall = data.rain->yesterday_rainfall;
            settings.setValue("rain/yesterday_rainfall", current_data.m_yesterday_rainfall);
            settings.sync();
        break;

    case wmr928::type::wind:
        this->current_data.m_wind_direction = data.wind->wind_direction;
            settings.setValue("wind/wind_direction", current_data.m_wind_direction);
        this->current_data.m_wind_speed_gust = data.wind->gust_wind_speed;
            settings.setValue("wind/wind_speed_gust", current_data.m_wind_speed_gust);
        this->current_data.m_wind_speed_average = data.wind->average_wind_speed;
            settings.setValue("wind/wind_speed_average", current_data.m_wind_speed_average);
        this->current_data.m_wind_chill = data.wind->wind_chill;
            settings.setValue("wind/wind_chill", data.wind->wind_chill);
        this->current_data.m_windLowBatt = data.wind->low_battery;
            settings.setValue("wind/wind_low_battery", current_data.m_windLowBatt);
            settings.sync();
        break;

    case wmr928::type::mushroom:
        this->current_data.m_outside_humidity = data.mushroom->humidity;
            settings.setValue("mushroom/outside_humidity", current_data.m_outside_humidity);
        this->current_data.m_outside_temperature = data.mushroom->temp;
            settings.setValue("mushroom/outside_temperature", current_data.m_outside_temperature);
        this->current_data.m_outside_dew_temp = data.mushroom->dew_temp;
            settings.setValue("mushroom/outside_dew_temp", current_data.m_outside_dew_temp);
        this->current_data.m_mushroomLowBatt = data.mushroom->low_battery;
            settings.sync();
        break;

    case wmr928::type::clock:
        break;

    case wmr928::type::thermo_only:
        break;

    case wmr928::type::thermo_hygro_baro2:
        this->current_data.m_inside_humidity = data.thermo_hygro_baro2->humidity;
            settings.setValue("thermo_hygro_baro2/inside_humidity", current_data.m_inside_humidity);
        this->current_data.m_inside_temperature = data.thermo_hygro_baro2->temp;
            settings.setValue("thermo_hygro_baro2/inside_temperature", current_data.m_inside_temperature);
        this->current_data.m_inside_dew_temp = data.thermo_hygro_baro2->dew_temp;
            settings.setValue("thermo_hygro_baro2/inside_dew_temp", current_data.m_inside_dew_temp);
        this->current_data.m_pressure = data.thermo_hygro_baro2->adc0_baro_reading + (data.thermo_hygro_baro2->ADCbit9 << 8) + 600;
            settings.setValue("thermo_hygro_baro2/pressure", current_data.m_pressure);
        this->current_data.m_thermoHygroBaro2LowBatt = data.thermo_hygro_baro2->low_battery;
            this->savePressure();
            settings.sync();
        break;

    default: break;
    }

    emit currentDataChanged();
}

uint16_t DataHandler::gen_crc16(const char *data, uint16_t size)
{
    auto CRC16 = 0x8005;
    uint16_t out = 0;
     int bits_read = 0, bit_flag;

     if(data == NULL)
         return 0;

     while(size > 0)
     {
         bit_flag = out >> 15;

         out <<= 1;
         out |= (*data >> bits_read) & 1;

         bits_read++;
         if(bits_read > 7)
         {
             bits_read = 0;
             data++;
             size--;
         }

         if(bit_flag)
             out ^= CRC16;

     }

     int i;
     for (i = 0; i < 16; ++i) {
         bit_flag = out >> 15;
         out <<= 1;
         if(bit_flag)
             out ^= CRC16;
     }

     uint16_t crc = 0;
     i = 0x8000;
     int j = 0x0001;
     for (; i != 0; i >>=1, j <<= 1) {
         if (i & out) crc |= j;
     }

     return crc;
}

float DataHandler::getPressure(int hours)
{
    file = new QFile("pressure.csv");

    if(!file->open(QIODevice::ReadOnly))
    {
        return 0;
    }

    QTextStream in(file);
    QString line;
    qint64 secs = QDateTime::currentSecsSinceEpoch();

    do {
        line = in.readLine();
        if(line.split('\t').size() > 1)
        {
            if(line.split('\t')[1].toInt() <= (secs - hours * 3550) && line.split('\t')[1].toInt() >= (secs - hours * 3650))
            {
                return line.split('\t')[1].toInt();
            }
        }
    } while (!line.isNull());

    return 0;
}

void DataHandler::savePressure()
{
    file = new QFile("pressure.csv");

    if(!file->open(QIODevice::WriteOnly | QIODevice::Append))
    {
        return;
    }

    if(this->current_data.m_pressure >= 0)
    {
        QTextStream flux(file);
        flux.setCodec("UTF-8");
        flux << QDateTime::currentSecsSinceEpoch() << "\t" << this->current_data.m_pressure << "\r\n";
        file->close();
    }

}

void DataHandler::sendConfig(int numFrame, int numField, int dataToSend) // config w/ any data
{
    std::string str = "$";
    str.append(std::to_string(numFrame));
    str.append(",");
    str.append(std::to_string(numField));
    str.append(",");
    str.append(",");
    str.append(std::to_string(dataToSend));
    str.append(",");

    str.append(std::to_string(this->gen_crc16(str.data(), str.size())));

    str.append("*");

    TCP_Client.sendFrame(QString::fromStdString(str));
}

void DataHandler::sendConfig(int numFrame, int numField, double min, double max) // config w/ min max
{
    std::string str = "$";
    str.append(std::to_string(numFrame));
    str.append(",");
    str.append(std::to_string(numField));
    str.append(",");
    str.append(std::to_string(min).erase(5, 7));
    str.append(",");
    str.append(std::to_string(max).erase(5, 7));
    str.append(",");

    str.append(std::to_string(this->gen_crc16(str.data(), str.size())));

    str.append("*");

    qDebug() << str.c_str();

    TCP_Client.sendFrame(QString::fromStdString(str));
}

void DataHandler::sendConfig(int alarmValue) // general alarm disabler
{
    std::string str = "$";
    str.append(std::to_string(alarmValue));
    str.append(",");

    str.append(std::to_string(this->gen_crc16(str.data(), str.size())));

    str.append("*");

    qDebug() << str.c_str();

    TCP_Client.sendFrame(QString::fromStdString(str));
}

void DataHandler::sendConfig(int numFrame, int numField) // alarm disabler
{
    std::string str = "$";
    str.append(std::to_string(numFrame));
    str.append(",");
    str.append(std::to_string(numField));
    str.append(",");
    str.append(",");
    str.append(",");

    str.append(std::to_string(this->gen_crc16(str.data(), str.size())));

    str.append("*");

    qDebug() << str.c_str();

    TCP_Client.sendFrame(QString::fromStdString(str));
}

void DataHandler::sendIP(QString port, QString IP)
{
    TCP_Client.setIP(port, IP);
    QObject::connect(&TCP_Client, SIGNAL(send(std::vector<unsigned char>)), this, SLOT(getFrame(std::vector<unsigned char>)));
}

bool DataHandler::isReceiving()
{
    if(TCP_Client.isConnectOk())
    {
        return true;
    }
    else return false;
}

data DataHandler::getCurrentData()
{
    return current_data;
}

void DataHandler::setCurrentData(data _data)
{
    current_data = _data;
}
