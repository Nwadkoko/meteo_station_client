/****************************************************************************
** Meta object code from reading C++ file 'tcp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../client/tcp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tcp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TCP_t {
    QByteArrayData data[15];
    char stringdata0[130];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TCP_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TCP_t qt_meta_stringdata_TCP = {
    {
QT_MOC_LITERAL(0, 0, 3), // "TCP"
QT_MOC_LITERAL(1, 4, 7), // "connect"
QT_MOC_LITERAL(2, 12, 0), // ""
QT_MOC_LITERAL(3, 13, 4), // "send"
QT_MOC_LITERAL(4, 18, 26), // "std::vector<unsigned char>"
QT_MOC_LITERAL(5, 45, 9), // "sendFrame"
QT_MOC_LITERAL(6, 55, 1), // "t"
QT_MOC_LITERAL(7, 57, 11), // "isConnectOk"
QT_MOC_LITERAL(8, 69, 12), // "getIpAddress"
QT_MOC_LITERAL(9, 82, 5), // "setIP"
QT_MOC_LITERAL(10, 88, 5), // "_port"
QT_MOC_LITERAL(11, 94, 3), // "_IP"
QT_MOC_LITERAL(12, 98, 11), // "isConnected"
QT_MOC_LITERAL(13, 110, 14), // "isDisconnected"
QT_MOC_LITERAL(14, 125, 4) // "read"

    },
    "TCP\0connect\0\0send\0std::vector<unsigned char>\0"
    "sendFrame\0t\0isConnectOk\0getIpAddress\0"
    "setIP\0_port\0_IP\0isConnected\0isDisconnected\0"
    "read"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TCP[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    1,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    1,   63,    2, 0x0a /* Public */,
       7,    0,   66,    2, 0x0a /* Public */,
       8,    0,   67,    2, 0x0a /* Public */,
       9,    2,   68,    2, 0x0a /* Public */,
      12,    0,   73,    2, 0x08 /* Private */,
      13,    0,   74,    2, 0x08 /* Private */,
      14,    0,   75,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Bool,
    QMetaType::QString,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   10,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TCP::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TCP *_t = static_cast<TCP *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connect(); break;
        case 1: _t->send((*reinterpret_cast< std::vector<unsigned char>(*)>(_a[1]))); break;
        case 2: _t->sendFrame((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: { bool _r = _t->isConnectOk();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { QString _r = _t->getIpAddress();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 5: _t->setIP((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->isConnected(); break;
        case 7: _t->isDisconnected(); break;
        case 8: _t->read(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TCP::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCP::connect)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (TCP::*)(std::vector<unsigned char> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TCP::send)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TCP::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TCP.data,
      qt_meta_data_TCP,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *TCP::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TCP::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TCP.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int TCP::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void TCP::connect()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void TCP::send(std::vector<unsigned char> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
