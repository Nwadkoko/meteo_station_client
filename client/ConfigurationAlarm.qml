import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: element
    width: parent.width * 0.32
    height: parent.height * 0.25

    Rectangle {
        id: rectangle
        width: 400
        color: "#494949"
        radius: 10
        anchors {
            fill: parent
        }

        Rectangle {
            id: rectangleTitle
            color: "#fb0000"
            radius: 10
            width: parent.width
            height: 25
            anchors {
                top: parent.top
            }

            Label {
                id: labelTemperatureModule
                x: 104
                y: 0
                color: "#000000"
                text: qsTr("Configuration alarme")
                font.pointSize: 14
            }
        }

        Label {
            id: labelAlarm
            text: "Activer alarmes"
            font.pointSize: 14
            anchors {
                top: parent.top
                topMargin: 35
                left: parent.left
                leftMargin: 5
            }
        }

        Switch {
            id: switchGeneralAlarm
            checked: true
            anchors {
                left: labelAlarm.right
                leftMargin: 5
                verticalCenter: labelAlarm.verticalCenter
            }
            onToggled: {
                if(switchGeneralAlarm.checked)
                {
                    dataHandler.sendConfig(42);
                }
                else
                {
                    dataHandler.sendConfig(420);
                }
            }
        }

        Button {
            text: "Envoyer"
            anchors {
                verticalCenter: parent.verticalCenter
                verticalCenterOffset: 90
                horizontalCenter: parent.horizontalCenter
                horizontalCenterOffset: 155
            }
            onClicked: {
            }
        }
    }
}
