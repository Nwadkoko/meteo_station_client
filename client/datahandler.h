#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include <QObject>
#include <QFile>
#include <QDateTime>
#include <QTimer>
#include <QSettings>

#include "tcp.h"

struct data {
    Q_GADGET
public:
    float           m_inside_temperature{ 0 },
                    m_outside_temperature{ 0 },
                    m_outside_dew_temp{ 0 },
                    m_inside_dew_temp{ 0 },
                    m_wind_chill{ 0 },
                    m_wind_speed_average{ 0 },
                    m_wind_speed_gust{ 0 },
                    m_pressure{ 0 },
                    m_total_rainfall{ 0 },
                    m_yesterday_rainfall{ 0 },
                    m_inside_humidity{ 0 },
                    m_outside_humidity{ 0 },
                    m_wind_direction{ 0 },
                    m_rain{ 0 };

    bool            m_windLowBatt{ false },
                    m_rainLowBatt{ false },
                    m_thermoHygroLowBatt{ false },
                    m_mushroomLowBatt{ false },
                    m_thermoOnlyLowBatt{ false },
                    m_thermoHygroBaroLowBatt{ false },
                    m_mainLowBatt{ false },
                    m_thermoHygroBaro2LowBatt{ false };

    Q_PROPERTY(float inside_temperature MEMBER m_inside_temperature)
    Q_PROPERTY(float inside_dew_temp MEMBER m_inside_dew_temp)
    Q_PROPERTY(float outside_temperature MEMBER m_outside_temperature)
    Q_PROPERTY(float outside_dew_temp MEMBER m_outside_dew_temp)
    Q_PROPERTY(float wind_chill MEMBER m_wind_chill)
    Q_PROPERTY(float inside_humidity MEMBER m_inside_humidity)
    Q_PROPERTY(float outside_humidity MEMBER m_outside_humidity)
    Q_PROPERTY(float wind_direction MEMBER m_wind_direction)
    Q_PROPERTY(float wind_speed_gust MEMBER m_wind_speed_gust)
    Q_PROPERTY(float wind_speed_average MEMBER m_wind_speed_average)
    Q_PROPERTY(float rain MEMBER m_rain)
    Q_PROPERTY(float total_rainfall MEMBER m_total_rainfall)
    Q_PROPERTY(float yesterday_rainfall MEMBER m_yesterday_rainfall)
    Q_PROPERTY(float pressure MEMBER m_pressure)
    Q_PROPERTY(bool windLowBatt MEMBER m_windLowBatt)
    Q_PROPERTY(bool rainLowBatt MEMBER m_rainLowBatt)
    Q_PROPERTY(bool thermoHygroLowBatt MEMBER m_thermoHygroLowBatt)
    Q_PROPERTY(bool mushroomLowBatt MEMBER m_mushroomLowBatt)
    Q_PROPERTY(bool thermoOnlyLowBatt MEMBER m_thermoOnlyLowBatt)
    Q_PROPERTY(bool thermoHygroBaroLowBatt MEMBER m_thermoHygroBaroLowBatt)
    Q_PROPERTY(bool mainLowBatt MEMBER m_mainLowBatt)
    Q_PROPERTY(bool thermoHygroBaro2LowBatt MEMBER m_thermoHygroBaro2LowBatt)
};

Q_DECLARE_METATYPE(data)

class DataHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(data current_data READ getCurrentData NOTIFY currentDataChanged)

public:
    DataHandler();
    void writeSettings(QVariant value);
    QVariant readSettings();
    void setCurrentData(data _data);
    data getCurrentData();
    void savePressure();

public slots: 
    void getFrame(std::vector<unsigned char> frame);
    uint16_t gen_crc16(const char *data, uint16_t size);
    float getPressure(int hours);
    void sendConfig(int numFrame, int numField, int dataToSend);
    void sendConfig(int numFrame, int numField, double min, double max);
    void sendConfig(int alarmValue);
    void sendConfig(int numFrame, int numField);
    void sendIP(QString port, QString IP);
    bool isReceiving();

private:
    std::vector<unsigned char> buffer;
    data current_data;
    TCP TCP_Client;
    QFile *file;
    QTimer *timer;
    QSettings settings;

signals:
    void currentDataChanged();
};

#endif // DATAHANDLER_H
